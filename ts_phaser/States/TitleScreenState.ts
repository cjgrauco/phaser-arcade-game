﻿module Game {
   export class TitleScreenState extends Phaser.State {
      game: Phaser.Game;

      constructor() {
         super();
      }

      titleScreenImage: Phaser.Sprite;


      preload() {


      }

      create() {
         this.titleScreenImage = this.add.sprite(0, 0, "star");
      }
   }
}