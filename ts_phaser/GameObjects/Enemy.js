var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Game;
(function (Game) {
    var Enemy = (function (_super) {
        __extends(Enemy, _super);
        function Enemy(game, x, y) {
            _super.call(this, game, x, y, "enemy", 0);
            this.game.physics.arcade.enable(this);
            this.body.collideWorldBounds = true;
            console.log("Enemy constructor");
            this.game = game;
            this.animations.add('left', [0, 1], 5, true);
            this.animations.add('right', [2, 3], 5, true);
            this.startingX = x;
            this.startingY = y;
        }
        Enemy.prototype.update = function () {
            //not hurt
            if (this.x >= this.startingX + 100) {
                this.body.velocity.x = -20;
                this.play('left');
            }
            if (this.x < this.startingX - 100) {
                this.body.velocity.x = 20;
                this.play('right');
            }
        };
        return Enemy;
    }(Phaser.Sprite));
    Game.Enemy = Enemy;
})(Game || (Game = {}));
//# sourceMappingURL=Enemy.js.map