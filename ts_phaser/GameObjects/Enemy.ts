﻿module Game {
   export class Enemy extends Phaser.Sprite {
      game: Phaser.Game;
      health: number;
      xp: number;
      startingX: number;
      startingY: number;
      state: Phaser.State;


      constructor(game: Phaser.Game, x: number, y: number) {
         super(game, x, y, "enemy", 0);
         this.game.physics.arcade.enable(this);
         this.body.collideWorldBounds = true;

         console.log("Enemy constructor");

         this.game = game;
         this.animations.add('left', [0, 1], 5, true);
         this.animations.add('right', [2, 3], 5, true);

         this.startingX = x;
         this.startingY = y;
      }

      update() {



         

         //not hurt
         if (this.x >= this.startingX + 100) {

            this.body.velocity.x = -20;
            this.play('left');
         }

         if (this.x < this.startingX - 100) {
            this.body.velocity.x = 20;
            this.play('right');
         }




      }


   }
}