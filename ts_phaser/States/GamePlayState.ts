﻿module Game {


   export class GamePlayState extends Phaser.State {
      game: Phaser.Game;

      player: Game.Player;
      scene: Game.Scene;

      platforms: Phaser.Group;
      enemies: Phaser.Group;

      ground: Phaser.Sprite;
      ledge: Phaser.Sprite;

      enemy: Game.Enemy;

      constructor() {
         super();
      }

      preload() {}



      create() {
         this.game.world.resize(3000, 600);
         this.game.physics.startSystem(Phaser.Physics.ARCADE);


         this.scene = new Scene(this.game, 0, 0);
         this.game.add.existing(this.scene);


         // Player
         this.player = new Player(this.game, 0, this.game.world.height - 150);
         this.game.add.existing(this.player);

    




         //Enemies
         this.enemies = this.game.add.group();
         this.enemies.enableBody = true;
         this.enemies.physicsBodyType = Phaser.Physics.ARCADE;
         

         this.spawnPlatforms();
         this.spawnEnemies();

      }

      update() {

         // Collision detection
         this.game.physics.arcade.collide(this.player, this.platforms);
         this.game.physics.arcade.collide(this.enemies, this.platforms);


         // Collision handling
         this.game.physics.arcade.overlap(this.player.bullets, this.enemies, this.enemyHit, null, this);
         this.game.physics.arcade.overlap(this.player, this.enemies, this.playerDead, null, this);
         this.game.physics.arcade.overlap(this.player.bullet, this.platforms, this.killBullet, null, this);


         // pitfalls
         if (this.player.y == 552) {
            this.player.kill();

         }

         this.player.body.velocity.x = 0;


      }

      killBullet(bullet: Phaser.Sprite, platform: Phaser.Sprite) {
         console.log("bullet hit ledge");
         bullet.kill();         

      }

      enemyHit(bullet: Phaser.Sprite, enemy: Game.Enemy) {


         if (enemy.health == 0) {
            enemy.kill();
            this.player.xp += enemy.xp;
            console.log("Enemy died, XP given: " + this.enemy.xp + " Total XP:" + this.player.xp);

         } else {
            console.log("Enemy hit: " + enemy.health);
            enemy.health -= 1;
         }

         bullet.kill();

      }

      playerDead(player: Game.Player, enemy: Game.Enemy) {
         console.log("Player dead");
         player.kill();

      }
     

      spawnEnemies() {

         this.enemy = new Enemy(this.game, 400, this.game.height - 160);
         this.game.add.existing(this.enemy);

         this.enemy.health = 3;
         this.enemy.xp = 100;
         this.enemy.body.velocity.y = 0;
         this.enemy.body.gravity.y = 500;
         this.enemy.body.velocity.x = 20;
         this.enemy.play('right');

         this.enemies.add(this.enemy);

         
         this.enemy = new Enemy(this.game, 600, this.game.height - 160);
         this.game.add.existing(this.enemy);

         this.enemy.health = 3;
         this.enemy.xp = 50;
         this.enemy.body.velocity.y = 0;
         this.enemy.body.gravity.y = 500;
         

         this.enemy.body.velocity.x = 20;
         this.enemy.play('right');

         this.enemies.add(this.enemy);


         this.enemy = new Enemy(this.game, 390, 252);
         this.enemy.health = 3;
         this.enemy.xp = 50;
         this.enemy.body.velocity.y = 0;
         this.enemy.body.gravity.y = 500;
         this.enemy.body.velocity.x = 20;
         this.enemy.play('right');

         this.enemies.add(this.enemy);
      }

      spawnPlatforms() {

         this.platforms = this.game.add.group();
         this.platforms.enableBody = true;

         //ground
         this.ground = this.platforms.create(0, this.game.world.height - 64, 'ground');
         this.ground.body.immovable = true;
         this.ground.scale.setTo(3, 2);


         this.ground = this.platforms.create(1400, this.game.world.height - 64, 'ground');
         this.ground.body.immovable = true;

         this.ground.scale.setTo(1.3, 2);


         //ledges
         this.ledge = this.platforms.create(0, this.game.world.height - 200, 'ground');
         this.ledge.body.immovable = true;
         this.ledge.scale.setTo(0.5, 0.5);


         this.ledge = this.platforms.create(570, this.game.world.height - 230, 'ground');
         this.ledge.body.immovable = true;
         this.ledge.scale.setTo(0.15, 0.5);

         this.game.add.tween(this.ledge).to({ y: this.game.world.height - 370 }, 2000, Phaser.Easing.Linear.None, true, 0, Number.MAX_VALUE, true);
         

         this.ledge = this.platforms.create(730, this.game.world.height - 230, 'ground');
         this.ledge.body.immovable = true;
         this.ledge.scale.setTo(0.15, 0.5);
         this.game.add.tween(this.ledge).to({ y: this.game.world.height - 370 }, 1700, Phaser.Easing.Linear.None, true, 0, Number.MAX_VALUE, true); 


         this.ledge = this.platforms.create(300, this.game.world.height - 300, 'ground');
         this.ledge.body.immovable = true;
         this.ledge.scale.setTo(0.5, 0.5);

      }


   }
}