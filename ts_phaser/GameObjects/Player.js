var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Game;
(function (Game) {
    var Player = (function (_super) {
        __extends(Player, _super);
        function Player(game, x, y) {
            _super.call(this, game, x, y, "dude", 4);
            this.game = game;
            this.bullets = this.game.add.group();
            this.game.physics.arcade.enable(this);
            this.body.gravity.y = 900;
            this.body.bounce.set(0);
            this.body.collideWorldBounds = true;
            this.game.camera.follow(this, Phaser.Camera.FOLLOW_PLATFORMER);
            this.RIGHT_ARROW = this.game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
            this.LEFT_ARROW = this.game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
            this.SPACEBAR = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
            this.LEFT_CTRL = this.game.input.keyboard.addKey(Phaser.Keyboard.CONTROL);
            this.X_KEY = this.game.input.keyboard.addKey(Phaser.Keyboard.X);
            this.Z_KEY = this.game.input.keyboard.addKey(Phaser.Keyboard.Z);
            this.animations.add('left', [0, 1, 2, 3], 10, true);
            this.animations.add('right', [5, 6, 7, 8], 10, true);
            this.xp = 0;
            this.levelThresh = 1000;
            this.level = 1;
            //Weapon
            this.nextFire = 0;
            this.fireRate = 350;
            this.bullets.enableBody = true;
            this.bullets.physicsBodyType = Phaser.Physics.ARCADE;
            this.bullets.createMultiple(30, 'star');
            this.bullets.setAll('checkWorldBounds', true);
            this.bullets.setAll('outOfBoundsKill', true);
            console.log("player constructor");
            //Teleport
            this.teleportStarted = false;
            this.teleportCooldown = 0;
        }
        Player.prototype.update = function () {
            // Movement
            if (this.RIGHT_ARROW.isDown) {
                this.body.velocity.x = 150;
                this.animations.play('right');
            }
            else if (this.LEFT_ARROW.isDown) {
                this.body.velocity.x = -150;
                this.animations.play('left');
            }
            else {
                this.animations.stop();
                this.frame = 4;
            }
            //Jumping
            if (this.SPACEBAR.isDown && this.body.touching.down) {
                this.body.velocity.y = -550;
            }
            //Shooting
            if (this.LEFT_CTRL.isDown) {
                this.fireBullet();
            }
            if (this.X_KEY.isDown) {
                console.log("Player X: " + this.x + "Player Y: " + this.y);
            }
            if (this.Z_KEY.isDown && this.teleportStarted == false) {
                this.startTeleportation();
            }
        };
        Player.prototype.startTeleportation = function () {
            console.log("Starting teleportation");
            this.teleportStarted = true;
            this.teleportCooldown = this.game.time.now + 300;
            this.teleportBall = new Phaser.Sprite(this.game, this.x, this.y, "teleport");
            if (this.game.time.now > this.teleportCooldown) {
                this.game.add.existing(this.teleportBall);
            }
            this.teleportStarted = false;
        };
        Player.prototype.fireBullet = function () {
            if (this.game.time.now > this.nextFire && this.bullets.countDead() > 0) {
                console.log("Fireing bullet");
                this.nextFire = this.game.time.now + this.fireRate;
                if (this.RIGHT_ARROW.isDown) {
                    this.bullet = this.bullets.getFirstDead();
                    this.bullet.reset(this.x + 16, this.y + 16);
                    this.bullet.body.velocity.x = 450;
                }
                if (this.LEFT_ARROW.isDown) {
                    this.bullet = this.bullets.getFirstDead();
                    this.bullet.reset(this.x - 16, this.y + 16);
                    this.bullet.body.velocity.x = -500;
                }
            }
        };
        Player.prototype.render = function () {
        };
        return Player;
    }(Phaser.Sprite));
    Game.Player = Player;
})(Game || (Game = {}));
//# sourceMappingURL=Player.js.map