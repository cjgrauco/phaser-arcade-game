﻿module Game {
   export class ArcadeGame {
      game: Phaser.Game;



      constructor() {
         this.game = new Phaser.Game(800, 600, Phaser.AUTO, 'content', {
            create: this.create, preload: this.preload
         });

      }
      preload() {
         this.game.load.image('platform', "Graphics/platform.png");
         this.game.load.image('star', "Graphics/star.png");
         this.game.load.image('ground', "Graphics/platform.png");
         this.game.load.image('scene', "Graphics/sky.png");

         this.game.load.spritesheet('dude', "Graphics/dude.png", 32, 48);
         this.game.load.spritesheet('enemy', "Graphics/baddie.png", 32, 32);
         this.game.load.spritesheet('teleport', "Graphics/teleport.png", 24, 24);
      }



      create() {


         //this.game.state.add("TitleScreenState", Game.TitleScreenState, true);
         this.game.state.add("GamePlayState", Game.GamePlayState, true);
         
      }


   }
}

window.onload = () => {
   var game = new Game.ArcadeGame();
}