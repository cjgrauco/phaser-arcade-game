var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Game;
(function (Game) {
    var Scene = (function (_super) {
        __extends(Scene, _super);
        function Scene(game, x, y) {
            _super.call(this, game, x, y, "scene", 0);
            this.nextFrame = new Phaser.Sprite(this.game, this.width, 0, "scene", 0);
            this.game.add.existing(this.nextFrame);
        }
        Scene.prototype.update = function () {
        };
        return Scene;
    }(Phaser.Sprite));
    Game.Scene = Scene;
})(Game || (Game = {}));
//# sourceMappingURL=Scene.js.map